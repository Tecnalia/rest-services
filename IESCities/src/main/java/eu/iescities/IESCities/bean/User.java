package eu.iescities.IESCities.bean;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="User")
@XmlType(propOrder={"user_id","username","password","name","surname","email","profile","admin","developer",
		"council_id","applications","dataset","comment"})
public class User {
	
	private int user_id;	
	private String username;
	private String password;
	private String name;
	private String surname;
	private String email;
	private String profile;
	private int council_id;
	private boolean admin;
	private boolean developer;

	private ArrayList<Application> applications = new ArrayList<Application> ();
	private ArrayList<Dataset> dataset = new ArrayList<Dataset> ();
	private ArrayList<Comment> comment = new ArrayList<Comment> ();
	
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public boolean isDeveloper() {
		return developer;
	}
	public void setDeveloper(boolean developer) {
		this.developer = developer;
	}

	public ArrayList<Comment> getComment() {
		return comment;
	}
	public void addComment(Comment comment) {
		this.comment.add(comment);
	}
	public void setComment(ArrayList<Comment> comment) {
		this.comment = comment;
	}

	public ArrayList<Dataset> getDataset() {
		return dataset;
	}
	public void addDataset(Dataset dataset) {
		this.dataset.add(dataset);
	}
	public void setDataset(ArrayList<Dataset> dataset) {
		this.dataset = dataset;
	}

	public ArrayList<Application> getApplications() {
		return applications;
	}
	public void addApplication(Application application) {
		this.applications.add(application);
	}
	
	public void setApplications(ArrayList<Application> applications) {
		this.applications = applications;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public int getCouncil_id() {
		return council_id;
	}
	public void setCouncil_id(int council_id) {
		this.council_id = council_id;
	}
	
	
}
