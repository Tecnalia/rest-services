package eu.iescities.IESCities.bean;

public class StatisticalData {
	private int statistical_id;
	private String name;
	
	
	public StatisticalData() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StatisticalData(int statistical_id, String name) {
		super();
		this.statistical_id = statistical_id;
		this.name = name;
	}
	public int getStatistical_id() {
		return statistical_id;
	}
	public void setStatistical_id(int statistical_id) {
		this.statistical_id = statistical_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
