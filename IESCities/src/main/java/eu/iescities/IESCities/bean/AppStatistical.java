package eu.iescities.IESCities.bean;

public class AppStatistical {
	private int app_id;
	private int statistical_id;
	private String value;
	
	public AppStatistical() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AppStatistical(int app_id, int statistical_id, String value) {
		super();
		this.app_id = app_id;
		this.statistical_id = statistical_id;
		this.value = value;
	}
	public int getApp_id() {
		return app_id;
	}
	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}
	public int getStatistical_id() {
		return statistical_id;
	}
	public void setStatistical_id(int statistical_id) {
		this.statistical_id = statistical_id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
