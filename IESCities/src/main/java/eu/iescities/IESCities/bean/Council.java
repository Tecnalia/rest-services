package eu.iescities.IESCities.bean;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="Council")
@XmlType(propOrder={"council_id","name","description"})
public class Council {
	private int council_id;
	private String name;
	private String description;
	
	public int getCouncil_id() {
		return council_id;
	}
	public void setCouncil_id(int council_id) {
		this.council_id = council_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
