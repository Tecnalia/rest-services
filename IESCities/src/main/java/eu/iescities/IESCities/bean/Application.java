package eu.iescities.IESCities.bean;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="Application")
@XmlType(propOrder={"app_id","name","description","url","image","tag","geographical_scope",
		"version","toS","access_number","download_number","dataset","comment"})
public class Application {
	private int app_id;
	private String name;
	private String description;
	private String url;
	private String image;
	private String geographical_scope;
	private String version;
	private String toS;
	private int access_number;
	private int download_number;
	private ArrayList<Dataset> dataset = new ArrayList<Dataset> ();
	private ArrayList<Comment> comment = new ArrayList<Comment> ();
	private ArrayList<Tag> tag = new ArrayList<Tag> ();
	
	public void addTag(Tag tag) {
		this.tag.add(tag);
	}	
	public ArrayList<Tag> getTag() {
		return tag;
	}
	public void setTag(ArrayList<Tag> tag) {
		this.tag = tag;
	}
	public ArrayList<Comment> getComment() {
		return comment;
	}
	public void addComment(Comment comment) {
		this.comment.add(comment);
	}
	public void setComment(ArrayList<Comment> comment) {
		this.comment = comment;
	}

	public ArrayList<Dataset> getDataset() {
		return dataset;
	}
	public void addDataset(Dataset dataset) {
		this.dataset.add(dataset);
	}
	public void setDataset(ArrayList<Dataset> dataset) {
		this.dataset = dataset;
	}
	
	public int getApp_id() {
		return app_id;
	}
	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getGeographical_scope() {
		return geographical_scope;
	}
	public void setGeographical_scope(String geographical_scope) {
		this.geographical_scope = geographical_scope;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getToS() {
		return toS;
	}
	public void setToS(String toS) {
		this.toS = toS;
	}
	public int getAccess_number() {
		return access_number;
	}
	public void setAccess_number(int access_number) {
		this.access_number = access_number;
	}
	public int getDownload_number() {
		return download_number;
	}
	public void setDownload_number(int download_number) {
		this.download_number = download_number;
	}
	
}
