package eu.iescities.IESCities.bean;

public class DatasetStatistical {
	private int dataset_id;
	private int statistical_id;
	
	public DatasetStatistical() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DatasetStatistical(int dataset_id, int statistical_id) {
		super();
		this.dataset_id = dataset_id;
		this.statistical_id = statistical_id;
	}
	public int getDataset_id() {
		return dataset_id;
	}
	public void setDataset_id(int dataset_id) {
		this.dataset_id = dataset_id;
	}
	public int getStatistical_id() {
		return statistical_id;
	}
	public void setStatistical_id(int statistical_id) {
		this.statistical_id = statistical_id;
	}
	
	
}
