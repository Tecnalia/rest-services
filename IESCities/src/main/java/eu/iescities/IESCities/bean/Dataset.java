package eu.iescities.IESCities.bean;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="Dataset")
@XmlType(propOrder={"dataset_id","user_id","council_id","description","read_access_number",
		"write_access_number","number_apps_useit","publishing_timestamp"})
public class Dataset {
	private int dataset_id;
	private int user_id;
	private int council_id;
	private String description;
	private int read_access_number;
	private int write_access_number;
	private int number_apps_useit;
	private String publishing_timestamp;
	
	public int getCouncil_id() {
		return council_id;
	}
	public void setCouncil_id(int council_id) {
		this.council_id = council_id;
	}
	public String getPublishing_timestamp() {
		return publishing_timestamp;
	}
	public void setPublishing_timestamp(String publishing_timestamp) {
		this.publishing_timestamp = publishing_timestamp;
	}
	public int getDataset_id() {
		return dataset_id;
	}
	public void setDataset_id(int dataset_id) {
		this.dataset_id = dataset_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getRead_access_number() {
		return read_access_number;
	}
	public void setRead_access_number(int read_access_number) {
		this.read_access_number = read_access_number;
	}
	public int getWrite_access_number() {
		return write_access_number;
	}
	public void setWrite_access_number(int write_access_number) {
		this.write_access_number = write_access_number;
	}
	public int getNumber_apps_useit() {
		return number_apps_useit;
	}
	public void setNumber_apps_useit(int number_apps_useit) {
		this.number_apps_useit = number_apps_useit;
	}
	
}
