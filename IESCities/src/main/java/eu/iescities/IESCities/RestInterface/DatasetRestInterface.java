package eu.iescities.IESCities.RestInterface;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import eu.iescities.IESCities.bean.Dataset;
import eu.iescities.IESCities.bean.User;

@Path("/DatasetRestInterface")
public class DatasetRestInterface {
	
    @GET 
    @Path ("/createdataset.json")
    @Produces("application/json")
    public Dataset CreateDataset(@QueryParam("description") String description, @QueryParam("userid") String userid	) {
    	Dataset fake = new Dataset();
    	fake.setDataset_id(1);
    	fake.setDescription(description);
    	fake.setUser_id(new Integer(userid).intValue());
        return fake;
    }
    @GET 
    @Path ("/deletedataset.json")
    @Produces("application/json")
    public boolean DeleteDataset(@QueryParam("id") String id	) {
         return true;
    }
    @GET 
    @Path ("/updatedataset.json")
    @Produces("application/json")
    public Dataset UpdateDataset(@QueryParam("id") String id, @QueryParam("description") String description, 
    		@QueryParam("number_apps_useit") String number_apps_useit,
    		@QueryParam("read_access_number") String read_access_number) {
    	Dataset fake = new Dataset();
    	fake.setDataset_id(new Integer(id).intValue());
    	fake.setNumber_apps_useit(new Integer(number_apps_useit).intValue());
    	fake.setRead_access_number(new Integer(read_access_number).intValue());
        return fake;
    }
    @GET 
    @Path ("/getalldataset.json")
    @Produces("application/json")
    public ArrayList<Dataset> GetAllDataset() {
    	Dataset fake = new Dataset();
    	fake.setDataset_id(1);
    	fake.setDescription("Description1");
    	Dataset fake2 = new Dataset();
    	fake2.setDataset_id(2);
    	fake2.setDescription("Description2");
    	ArrayList<Dataset> lista = new ArrayList<Dataset>();
    	lista.add(fake);
    	lista.add(fake2);
        return lista;
    }
    @GET 
    @Path ("/searchdataset.json")
    @Produces("application/json")
    public ArrayList<Dataset> SearchDataset(@QueryParam("busqueda") String busqueda	) {
    	Dataset fake = new Dataset();
    	fake.setDataset_id(1);
    	fake.setDescription("Description1");
    	Dataset fake2 = new Dataset();
    	fake2.setDataset_id(2);
    	fake2.setDescription("Description2");
    	ArrayList<Dataset> lista = new ArrayList<Dataset>();
    	lista.add(fake);
    	lista.add(fake2);
        return lista;
    }   
    @GET 
    @Path ("/getdatasetdeveloper.json")
    @Produces("application/json")
    public User GetDatasetDeveloper(@QueryParam("id") String id	) {
       	User userfake = new User();
    	userfake.setName("Name");
    	//userfake.setPassword(password);
    	userfake.setSurname("Surname");
    	userfake.setUsername("Username");
    	userfake.setEmail("Mail@email.com");
    	userfake.setProfile("Council");
    	userfake.setCouncil_id(1);
        return userfake;
    }       
}
