package eu.iescities.IESCities.RestInterface;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import eu.iescities.IESCities.bean.User;
import eu.iescities.IESCities.bean.Application;

@Path("/UserRestInterface")
public class UserRestInterface {
	private AppRestInterface appinterfe= null;
	
    @GET 
    @Path ("/loginUser.json")
    @Produces("application/json")
    public User LoginUser(@QueryParam("user") String user, @QueryParam("pass") String pass   		) {
    	User userfake = new User();
    	userfake.setName(user);
    	userfake.setSurname(pass);
    	userfake.setProfile("Developer");
    	Application appfake = new Application();
    	appfake.setDescription("Fake application 1");
    	appfake.setName("Name app one");
    	//appfake.setTags("tag1;tag2;tag3");
    	appfake.setApp_id(1);
    	appfake.setUrl("http://url");
    	appfake.setToS("TOS app1");
    	userfake.addApplication(appfake);
    	Application appfake2 = new Application();
    	appfake2.setDescription("Fake application 2");
    	appfake2.setName("Name app two");
    	//appfake2.setTags("tag1;tag2;tag3");
    	appfake2.setApp_id(1);
    	appfake2.setUrl("http://url2");
    	appfake2.setToS("TOS app1");
    	userfake.addApplication(appfake2);
        return userfake;
    }
    @GET 
    @Path ("/createuser.json")
    @Produces("application/json")
    public User CreateUser(@QueryParam("name") String name, @QueryParam("password") String password,
    		@QueryParam("surname") String surname, @QueryParam("username") String username,
    		@QueryParam("email") String email, @QueryParam("council_id") String council_id,
    		@QueryParam("profile") String profile) {
    	User userfake = new User();
    	userfake.setName(name);
    	userfake.setPassword(password);
    	userfake.setSurname(surname);
    	userfake.setUsername(username);
    	userfake.setEmail(email);
    	userfake.setProfile(profile);
    	userfake.setCouncil_id(new Integer(council_id).intValue());
        return userfake;
    }
    @GET 
    @Path ("/updateuser.json")
    @Produces("application/json")
    public User UpdateUser(@QueryParam("id") String id, @QueryParam("name") String name, @QueryParam("password") String password,
    		@QueryParam("surname") String surname, @QueryParam("username") String username,
    		@QueryParam("email") String email, @QueryParam("council_id") String council_id,
    		@QueryParam("profile") String profile) {
    	User userfake = new User();
    	userfake.setName(name);
    	userfake.setPassword(password);
    	userfake.setSurname(surname);
    	userfake.setUsername(username);
    	userfake.setEmail(email);
    	userfake.setProfile(profile);
    	userfake.setCouncil_id(new Integer(council_id).intValue());
        return userfake;
    } 
    @GET 
    @Path ("/addapptouser.json")
    @Produces("application/json")
    public User AddAppToUser(@QueryParam("userid") String userid, @QueryParam("appid") String appid) {
    	User userfake = this.GetUser(userid);
    	appinterfe = new AppRestInterface();
    	userfake.addApplication(appinterfe.GetApp(appid));
   
        return userfake;
    }  
    
    @GET 
    @Path ("/getuser.json")
    @Produces("application/json")
    public User GetUser(@QueryParam("id") String id) {
    	User userfake = new User();
    	userfake.setName("Jonh");
    	userfake.setSurname("dow");
    	userfake.setUsername("UserName");
    	userfake.setEmail("Jonh@test.com");
    	userfake.setProfile("Developer");
    	userfake.setCouncil_id(new Integer(id).intValue());
        return userfake;
    }
    @GET 
    @Path ("/deleteuser.json")
    @Produces("application/json")
    public boolean DeleteUser(@QueryParam("id") String id) {
    	
        return true;
    }	    
}
