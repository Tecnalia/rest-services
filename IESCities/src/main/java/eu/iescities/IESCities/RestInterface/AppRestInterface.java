package eu.iescities.IESCities.RestInterface;


import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import eu.iescities.IESCities.bean.Application;
import eu.iescities.IESCities.bean.Tag;

@Path("/AppRestInterface")
public class AppRestInterface {
	
	private UserRestInterface userinterfe= null;

    @GET 
    @Path ("/createapp.json")
    @Produces("application/json")
    public Application CreateApp(@QueryParam("name") String name, @QueryParam("userid") String userid,
    		@QueryParam("tags") String tags, @QueryParam("url") String url,
    		@QueryParam("tos") String tos,  @QueryParam("description") String description) {
		Application appfake = new Application();
		//
		userinterfe = new UserRestInterface();
		userinterfe.GetUser(userid).addApplication(appfake);
		//
		appfake.setDescription(description);
		appfake.setName(name);
		
		ArrayList<Tag> tag = new ArrayList<Tag>(); 
		ArrayList<String> tagstring = (ArrayList<String>) Arrays.asList(tags.split(";"));  
		for (String e : tagstring) 
	      {  
			tag.add(new Tag(e)); 
	      }  		
		appfake.setTag(tag);
		appfake.setApp_id(1);
		appfake.setUrl(url);
		appfake.setToS(tos);
		return appfake;
    }
    @GET 
    @Path ("/updateapp.json")
    @Produces("application/json")
    public Application UpdateApp(@QueryParam("appid") String appid, @QueryParam("name") String name, @QueryParam("userid") String userid,
    		@QueryParam("tags") String tags, @QueryParam("url") String url,
    		@QueryParam("tos") String tos,  @QueryParam("description") String description) {
		Application appfake = new Application();
		//
		userinterfe = new UserRestInterface();
		userinterfe.GetUser(userid).addApplication(appfake);
		//
		appfake.setDescription(description);
		appfake.setName(name);
		ArrayList<Tag> tag = new ArrayList<Tag>(); 
		ArrayList<String> tagstring = (ArrayList<String>) Arrays.asList(tags.split(":"));  
		for (String e : tagstring) 
	      {  
			tag.add(new Tag(e)); 
	      }  		
		appfake.setTag(tag);
		appfake.setApp_id(new Integer(appid));
		appfake.setUrl(url);
		appfake.setToS(tos);
		return appfake;
       
    }  
    @GET 
    @Path ("/getapp.json")
    @Produces("application/json")
    public Application GetApp(@QueryParam("id") String id) {
		Application appfake = new Application();
		appfake.setDescription("description");
		appfake.setName("name");
		ArrayList<Tag> tag = new ArrayList<Tag>(); 
		ArrayList<String> tagstring = (ArrayList<String>) Arrays.asList("tag1;tag2".split(":"));  
		for (String e : tagstring) 
	      {  
			tag.add(new Tag(e)); 
	      }  		
		appfake.setTag(tag);
		appfake.setApp_id(new Integer(id));
		appfake.setUrl("url");
		appfake.setToS("tos");
		return appfake;
   }
    @GET 
    @Path ("/getallapps.json")
    @Produces("application/json")
    public ArrayList<Application> GetAllApp() {
    	ArrayList<Application> apparray = new ArrayList<Application>();
		Application appfake = new Application();
		appfake.setDescription("description");
		appfake.setName("name");
		ArrayList<Tag> tag = new ArrayList<Tag>(); 
		ArrayList<String> tagstring = (ArrayList<String>) Arrays.asList("tag1:tag2".split(":"));  
		for (String e : tagstring) 
	      {  
			tag.add(new Tag(e)); 
	      }  		
		appfake.setTag(tag);
		appfake.setApp_id(new Integer(1));
		appfake.setUrl("url");
		appfake.setToS("tos");
		Application appfake2 = new Application();
		appfake2.setDescription("description2");
		appfake2.setName("name2");
		tagstring = (ArrayList<String>) Arrays.asList("tag3:tag4".split(":"));   
		for (String e : tagstring) 
	      {  
			tag.add(new Tag(e)); 
	      }  		
		appfake.setTag(tag);
		appfake2.setApp_id(new Integer(2));
		appfake2.setUrl("url2");
		appfake2.setToS("tos2");
		apparray.add(appfake);
		apparray.add(appfake2);
		return apparray;
   }    
    @GET 
    @Path ("/deleteapp.json")
    @Produces("application/json")
    public boolean DeleteApp(@QueryParam("id") String id) {
    	
        return true;
    }	    
}
