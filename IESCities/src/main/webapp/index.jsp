<html>
<head >
<%@ page import="java.net.InetAddress" %>
<title>IESCities RESTful Test Page</title>
<script type="text/javascript" src="lib/Base64.js"></script>
<script type="text/javascript">
<%

InetAddress localHost = InetAddress.getLocalHost();
System.out.println(localHost.getHostName());
System.out.println(localHost.getHostAddress()); 
String localip=localHost.getHostAddress();
%>
var xml;
var url;
function callWithoutBasicAuth(resource){
	url = 'http://<%=localip%>:8080/IESCities/webresources'+resource;
	xml = getHTTPObject();
	xml.onreadystatechange = procesarResultado;
	xml.open('GET',url)	;
	xml.send();
}
function callWithBasicAuth(user, password,resource){
	var auth = make_basic_auth(user,password);
	url = 'http://<%=localip%>:8080/IESCities/webresources'+resource;
	xml = getHTTPObject();
	xml.onreadystatechange = procesarResultado;
	xml.open('GET',url)	;
	xml.setRequestHeader('Authorization', auth);	
	xml.send();
}
function procesarResultado(){
	var result = document.getElementById("result");
	if (xml.readyState == 4)
		result.innerHTML = url +"</br></br>"+ xml.responseText;
	else
		result.innerHTML = "Loading...";
}
function getHTTPObject(){
	if (typeof XMLHttpRequest != 'undefined'){
		return new XMLHttpRequest();
	}
	try{
		return new ActiveXObject("Msxml2.XMLHTTP");
		
	}
	catch (e){
		try{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
			
		}
	}
}
function make_basic_auth(user, password){
	var tok = user + ':' + password;
	var hash = Base64.encode(tok);
	return "Basic " + hash;
}
</script>
</head>
<body>
<img src="img/sito-ies_logo-w940px1.png"/>
<!--  <p><a href="webresources/myresource/unsecure">Unsecure Jersey resource</a>-->
<table>
<tr>
<th scope="col" colspan="3">UserRestInterface</th>

</tr>
    <tr>
        <td><input type="button" onclick="callWithBasicAuth('me','mypassword','/myresource/secure');" value="Secure Jersey resource" /></td>
        <td>callWithBasicAuth('me','mypassword','/myresource/secure');</td>
    </tr>
    <tr>
        <td><input type="button" onclick="callWithoutBasicAuth('/myresource/unsecure');" value="Unsecure Jersey resource" /></td>
        <td>callWithoutBasicAuth('/myresource/unsecure');</td>
    </tr>
    <tr>
         <td><input type="button" onclick="callWithoutBasicAuth('/UserRestInterface/loginUser.json?user=Jonh&pass=doe');" value="Login (get user data) Example" /></td>
        <td>callWithoutBasicAuth('/UserRestInterface/loginUser.json?user=Jonh&pass=doe');</td>
    </tr>
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/UserRestInterface/createuser.json?name=Jonh&amp;password=doe&amp;surname=surname&amp;username=username&amp;email=email&amp;council_id=1&amp;profile=Developer');" value="Create User">	</td>
        <td>callWithoutBasicAuth('/UserRestInterface/createuser.json?name=Jonh&amp;password=doe&amp;surname=surname&amp;username=username&amp;email=email&amp;council_id=1&amp;profile=Developer');</td>
    </tr>
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/UserRestInterface/getuser.json?id=1');" value="Get User by ID"></td>
        <td>callWithoutBasicAuth('/UserRestInterface/getuser.json?id=1');</td>
    </tr>    
</table>

<table>
<tr>
<th scope="col" colspan="3">AppRestInterface</th>

</tr>
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/AppRestInterface/createapp.json?name=App1&amp;userid=1&amp;tags=tag1:tag2&amp;url=url&amp;tos=tos&amp;description=description');" value="Create App">	</td>
        <td>callWithoutBasicAuth('/AppRestInterface/createapp.json?name=App1&amp;userid=1&amp;tags=tag1:tag2&amp;url=url&amp;tos=tos&amp;description=description');</td>
    </tr>
 
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/AppRestInterface/updateapp.json?appid=1&amp;name=App1&amp;userid=1&amp;tags=tag1:tag2&amp;url=url&amp;tos=tos&amp;description=description');" value="Update App">	</td>
        <td>callWithoutBasicAuth('/AppRestInterface/updateapp.json?appid=appid&amp;name=App1&amp;userid=1&amp;tags=tag1:tag2&amp;url=url&amp;tos=tos&amp;description=description');</td>
    </tr>
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/AppRestInterface/getapp.json?id=1');" value="Get User by ID"></td>
        <td>callWithoutBasicAuth('/AppRestInterface/getapp.json?id=1');</td>
    </tr>   
    <tr>
        
        <td><input type="button" onclick="callWithoutBasicAuth('/AppRestInterface/getallapps.json');" value="Get User by ID"></td>
        <td>callWithoutBasicAuth('/AppRestInterface/getallapps.json');</td>
    </tr>    
</table>

<p>

<div id="result"></div>
</body>
</html>
